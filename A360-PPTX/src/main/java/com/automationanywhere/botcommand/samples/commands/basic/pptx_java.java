package com.automationanywhere.botcommand.samples.commands.basic;

import java.io.FileOutputStream;

import com.spire.presentation.*;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.Date;
import java.util.regex.*;

public class pptx_java {
    pptx_java() {
    }

    public String splitPPTX(String filePathPPT, String filePathExcel, String splitPPTFolderPath) throws Exception {
        String status = "";
        HSSFWorkbook workbook = null;
        HSSFSheet sheet;
        HSSFRow rowHead;

        try {
            //creating an instance of HSSFWorkbook class
            workbook = new HSSFWorkbook();
            sheet = workbook.createSheet("Sheet1");
            rowHead = sheet.createRow((short) 0);
            //creating cell by using the createCell() method and setting the values to the cell by using the setCellValue() method
            rowHead.createCell(0).setCellValue("Slide Number");
            rowHead.createCell(1).setCellValue("Industry");
            rowHead.createCell(2).setCellValue("Company Name");
            //Company Name is ID as seen in ppt
            rowHead.createCell(3).setCellValue("Submitter");
            rowHead.createCell(4).setCellValue("Region");
            rowHead.createCell(5).setCellValue("Country");
            rowHead.createCell(6).setCellValue("Content type");
            rowHead.createCell(7).setCellValue("Content sanitization status");
            rowHead.createCell(8).setCellValue("Split filename");

            FileOutputStream fileOut = new FileOutputStream(filePathExcel);
            workbook.write(fileOut);
            fileOut.close();

            //Load the sample PowerPoint file
            Presentation ppt = new Presentation();
            ppt.loadFromFile(filePathPPT);

            int slidesCount = ppt.getSlides().getCount();
            String titleName = "";

            for (int index = 0; index < slidesCount; index++) {
                //Create an instance of Presentation class
                Presentation newPPT = new Presentation();
                //Remove the default slide
                newPPT.getSlides().removeAt(0);

                //Select a slide from the source file and append it to the new document
                int slidePosition = newPPT.getSlides().append(ppt.getSlides().get(index));
                ISlide slide = newPPT.getSlides().get(slidePosition);
                String title = slide.getTitle();
                if (!title.trim().equals(""))
                    titleName = title.substring(0, 33);

                String domain = "", companyCode = "";
                for (Object shape : slide.getShapes()) {

                    if (shape instanceof IAutoShape) {
                        for (Object tp : ((IAutoShape) shape).getTextFrame().getParagraphs()) {

                            if (((IAutoShape) shape).getName().equals("TextBox 1")) {
                                if (domain.trim().equals(""))
                                    domain = ((ParagraphEx) tp).getText().replace('\\', '-').replace('/', '-');
                            }

                            Pattern pattern = Pattern.compile("[0-9A-Z]+-[A-Z]{2}");
                            Matcher matcher = pattern.matcher(((IAutoShape) shape).getTextFrame().getText());
                            while (matcher.find()) {
                                if (companyCode.trim().equals(""))
                                    companyCode = matcher.group();
                            }
                        }
                    }
                }

                //Save to file
                long timestamp = new Date().getTime();
                String fileName = "", fullFilePath = "";

                boolean isCompCodeAndDomainAbsent = companyCode.trim().equals("") && domain.trim().equals("");
                if (!isCompCodeAndDomainAbsent) {
                    fileName = "Usecase_" + domain + "_" + companyCode + "_" + timestamp + ".pptx";
                } else {
                    fileName = "Usecase_" + titleName + "_" + timestamp + ".pptx";
                }

                fullFilePath = splitPPTFolderPath + "/" + fileName;
                newPPT.saveToFile(fullFilePath, FileFormat.PPTX_2013);

                if (isCompCodeAndDomainAbsent)
                    continue;

                rowHead = sheet.createRow((short) index);
                //rowHead.createCell(0).setCellValue(((ISlide) slide).getSlideNumber());
                rowHead.createCell(0).setCellValue(index + 1);
                rowHead.createCell(1).setCellValue(domain);
                if (companyCode.trim().equals(""))
                    companyCode = "NA";
                rowHead.createCell(2).setCellValue(companyCode);
                rowHead.createCell(3).setCellValue("amit.mathur@automationanywhere.com");
                rowHead.createCell(4).setCellValue("IMEA");
                rowHead.createCell(5).setCellValue("India");
                rowHead.createCell(6).setCellValue("Use case");
                rowHead.createCell(7).setCellValue("Yes");
                rowHead.createCell(8).setCellValue(fileName);

                fileOut = new FileOutputStream(filePathExcel);
                workbook.write(fileOut);
                fileOut.close();
            }

            status = "Success";

        } catch (Exception e) {
            status = e.getMessage();
        } finally {
            workbook.close();
        }

        return status;
    }

    /*
    public static void killExcel() {
        try {
            Runtime.getRuntime().exec("taskkill /f /t /IM EXCEL.EXE");
            Thread.sleep(1000);
        } catch (IOException | InterruptedException ex) {
        }
    }
    */
}
