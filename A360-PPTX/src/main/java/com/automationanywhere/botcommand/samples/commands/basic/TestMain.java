package com.automationanywhere.botcommand.samples.commands.basic;

public class TestMain {
    public static void main(String[] args) throws Exception {
        String filepathPPTX = "C:\\Users\\pinkesh.achhodwala\\OneDrive - Automation Anywhere Software Private Limited\\Sales POC\\A360-PPTX\\IMEA Use Cases & Customer Stories - May 2021 - Copy.pptx";
        String filepathExcel = "C:\\Users\\pinkesh.achhodwala\\OneDrive - Automation Anywhere Software Private Limited\\Sales POC\\A360-PPTX\\output.xls";
        String folderPath = "C:\\Users\\pinkesh.achhodwala\\OneDrive - Automation Anywhere Software Private Limited\\Sales POC\\A360-PPTX\\splitFiles";

        pptx_java obj = new pptx_java();
        String result = obj.splitPPTX(filepathPPTX,filepathExcel,folderPath);

        System.out.println(result);

    }
}
