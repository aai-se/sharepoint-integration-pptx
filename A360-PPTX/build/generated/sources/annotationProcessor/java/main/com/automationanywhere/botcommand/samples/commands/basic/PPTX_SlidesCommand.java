package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class PPTX_SlidesCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(PPTX_SlidesCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    PPTX_Slides command = new PPTX_Slides();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("filepathPPTX") && parameters.get("filepathPPTX") != null && parameters.get("filepathPPTX").get() != null) {
      convertedParameters.put("filepathPPTX", parameters.get("filepathPPTX").get());
      if(convertedParameters.get("filepathPPTX") !=null && !(convertedParameters.get("filepathPPTX") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filepathPPTX", "String", parameters.get("filepathPPTX").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("filepathPPTX") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","filepathPPTX"));
    }

    if(parameters.containsKey("filepathExcel") && parameters.get("filepathExcel") != null && parameters.get("filepathExcel").get() != null) {
      convertedParameters.put("filepathExcel", parameters.get("filepathExcel").get());
      if(convertedParameters.get("filepathExcel") !=null && !(convertedParameters.get("filepathExcel") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filepathExcel", "String", parameters.get("filepathExcel").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("filepathExcel") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","filepathExcel"));
    }

    if(parameters.containsKey("folderpath") && parameters.get("folderpath") != null && parameters.get("folderpath").get() != null) {
      convertedParameters.put("folderpath", parameters.get("folderpath").get());
      if(convertedParameters.get("folderpath") !=null && !(convertedParameters.get("folderpath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","folderpath", "String", parameters.get("folderpath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("folderpath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","folderpath"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("filepathPPTX"),(String)convertedParameters.get("filepathExcel"),(String)convertedParameters.get("folderpath")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
